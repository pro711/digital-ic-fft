/* coding: utf-8 */
/* vim:set et sts=2 ts=2 sw=2: */

/*
 * testbench.v
 * 
 * Copyright (c) 2011 Tao Chen <08300720447@fudan.edu.cn>
 * Copyright (c) 2011 Peiqi Zheng <08300720459@fudan.edu.cn>
 *  
 */

//==============================================================================
// testbench for fft8 module.
//==============================================================================

`timescale 1ns/10ps

module testbench
  ();

  parameter Tclk = 10;
  localparam data_width = 4;

  reg clk, rst_n;
  reg [2*data_width-1:0] in [7:0];
  wire [2*data_width*8-1:0] in_1d;  // packed array
  reg en;
  wire [2*data_width-1:0] out [7:0];
  wire [2*data_width*8-1:0] out_1d;
  wire data_valid;
  assign in_1d = {in[7], in[6], in[5], in[4], in[3], in[2], in[1], in[0]};
  assign {out[7], out[6], out[5], out[4], out[3], out[2], out[1], out[0]} = out_1d;
  
  fft8
    #(.data_width(4))
  fft8_inst
    (.clk(clk), .rst_n(rst_n), .in(in_1d), .en(en), .out(out_1d), .data_valid(data_valid));

  // clock generation
  always
  begin
    clk <= 1'b1;
    #(Tclk/2);
    clk <= 1'b0;
    #(Tclk/2);
  end

/*
  integer r;
  
  // output tracker
  always @(req)
  begin
    if (reset)
    begin
      #0;
      $display("Timestamp = ", $time);
      $display("Reversed = %b", reverse);
      $display("Request matrix & Grant matrix");
      for(r = 0; r < num_ports; r = r + 1)
      begin
        $write("%b  ", req_2d[r]);
        $write("%b\n", gnt_2d[r]);
      end
      $write("\n");
    end
  end

  integer i;
*/
  
  initial
  begin
    // initialization
    rst_n = 1'b0;
    en = 1'b0;
    #(Tclk/2);
    rst_n = 1'b1;
    #Tclk;
    // zero input, output should be zero
    in[0] = 0;
    in[1] = 0;
    in[2] = 0;
    in[3] = 0;
    in[4] = 0;
    in[5] = 0;
    in[6] = 0;
    in[7] = 0;
    #Tclk;
    en = 1'b1;
    #(Tclk*2);
    // DC input
    in[0] = 8'h10;
    in[1] = 8'h10;
    in[2] = 8'h10;
    in[3] = 8'h10;
    in[4] = 8'h10;
    in[5] = 8'h10;
    in[6] = 8'h10;
    in[7] = 8'h10;
    #(Tclk*3);
    // DC on imaginary part
    in[0] = 8'h01;
    in[1] = 8'h01;
    in[2] = 8'h01;
    in[3] = 8'h01;
    in[4] = 8'h01;
    in[5] = 8'h01;
    in[6] = 8'h01;
    in[7] = 8'h01;
    #(Tclk*3);
    // DC on both real and imaginary parts
    in[0] = 8'h11;
    in[1] = 8'h11;
    in[2] = 8'h11;
    in[3] = 8'h11;
    in[4] = 8'h11;
    in[5] = 8'h11;
    in[6] = 8'h11;
    in[7] = 8'h11;
    #(Tclk*3);
    // sinusoidal signal, angular frequency 1/8*2*pi
    in[0] = 8'b00000000;
    in[1] = 8'b01000000;
    in[2] = 8'b01100000;
    in[3] = 8'b01000000;
    in[4] = 8'b00000000;
    in[5] = 8'b11010000;
    in[6] = 8'b10110000;
    in[7] = 8'b11010000;
    #(Tclk*3);
    // sinusoidal signal, angular frequency 3/8*2*pi
    in[0] = 8'b00000000;
    in[1] = 8'b01000000;
    in[2] = 8'b10110000;
    in[3] = 8'b01000000;
    in[4] = 8'b00000000;
    in[5] = 8'b11010000;
    in[6] = 8'b01100000;
    in[7] = 8'b11010000;
    #100;
    
/*
    $finish;
*/
  end

endmodule
