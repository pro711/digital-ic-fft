vlib work
vmap work work
vlog -work work ../../src/control.v
vlog -work work ../../src/fft8.v
vlog -work work ../../src/butterfly.v
vlog -work work ../../src/crossbar.v
vlog -work work ../../src/rotate.v
vlog -work work ../../src/adder.v
vlog -work work ../../src/c_adder.v
vlog -work work ../../src/c_mult.v
vlog -work work ../../src/c_negate.v
vlog -work work testbench.v
vsim -lib work -novopt work.testbench
view signals
add wave sim:/testbench/*
add wave sim:/testbench/in
add wave sim:/testbench/fft8_inst/out_a
add wave sim:/testbench/fft8_inst/out_b
add wave sim:/testbench/fft8_inst/xbar/in_2d
add wave sim:/testbench/fft8_inst/xbar/out_2d
add wave sim:/testbench/fft8_inst/xbar/order
add wave sim:/testbench/fft8_inst/bf_0/w
add wave sim:/testbench/fft8_inst/bf_0/in_a
add wave sim:/testbench/fft8_inst/bf_0/in_b
add wave sim:/testbench/fft8_inst/bf_0/out_a
add wave sim:/testbench/fft8_inst/bf_0/out_b
add wave sim:/testbench/fft8_inst/bf_1/w
add wave sim:/testbench/fft8_inst/bf_1/in_a
add wave sim:/testbench/fft8_inst/bf_1/in_b
add wave sim:/testbench/fft8_inst/bf_1/out_a
add wave sim:/testbench/fft8_inst/bf_1/out_b
add wave sim:/testbench/fft8_inst/bf_2/w
add wave sim:/testbench/fft8_inst/bf_2/in_a
add wave sim:/testbench/fft8_inst/bf_2/in_b
add wave sim:/testbench/fft8_inst/bf_2/out_a
add wave sim:/testbench/fft8_inst/bf_2/out_b
add wave sim:/testbench/fft8_inst/bf_3/w
add wave sim:/testbench/fft8_inst/bf_3/in_a
add wave sim:/testbench/fft8_inst/bf_3/in_b
add wave sim:/testbench/fft8_inst/bf_3/out_a
add wave sim:/testbench/fft8_inst/bf_3/out_b

property wave -radix hex /testbench/in
property wave -radix hex /testbench/out
property wave -radix hex /testbench/fft8_inst/xbar/in_2d
property wave -radix hex /testbench/fft8_inst/xbar/out_2d
property wave -radix hex /testbench/fft8_inst/bf_0/w
property wave -radix hex /testbench/fft8_inst/bf_0/in_a
property wave -radix hex /testbench/fft8_inst/bf_0/in_b
property wave -radix hex /testbench/fft8_inst/bf_0/out_a
property wave -radix hex /testbench/fft8_inst/bf_0/out_b
property wave -radix hex /testbench/fft8_inst/bf_1/w
property wave -radix hex /testbench/fft8_inst/bf_1/in_a
property wave -radix hex /testbench/fft8_inst/bf_1/in_b
property wave -radix hex /testbench/fft8_inst/bf_1/out_a
property wave -radix hex /testbench/fft8_inst/bf_1/out_b
property wave -radix hex /testbench/fft8_inst/bf_2/w
property wave -radix hex /testbench/fft8_inst/bf_2/in_a
property wave -radix hex /testbench/fft8_inst/bf_2/in_b
property wave -radix hex /testbench/fft8_inst/bf_2/out_a
property wave -radix hex /testbench/fft8_inst/bf_2/out_b
property wave -radix hex /testbench/fft8_inst/bf_3/w
property wave -radix hex /testbench/fft8_inst/bf_3/in_a
property wave -radix hex /testbench/fft8_inst/bf_3/in_b
property wave -radix hex /testbench/fft8_inst/bf_3/out_a
property wave -radix hex /testbench/fft8_inst/bf_3/out_b

run 120ns


