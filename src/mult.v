/* coding: utf-8 */
/* vim:set et sts=2 ts=2 sw=2: */

/*
 * mult.v
 *  
 * Copyright (c) 2011 Tao Chen <08300720447@fudan.edu.cn>
 * Copyright (c) 2011 Peiqi Zheng <08300720459@fudan.edu.cn>
 *  
 */

`timescale 1ns/10ps

//==============================================================================
// multiplier for use in FFT with saturation operation
//==============================================================================

module mult(a, b, p);

  parameter width = 4;

  input signed [width-1:0] a;
  input signed [width-1:0] b;
  output signed [width-1:0] p;
  
  wire signed [2*width-1:0] p_full;

  c_mult
    #(.width(width))
    mult4
    (.a(a),
     .b(b),
     .p(p_full));
    

  assign p = a * b;
  
endmodule
