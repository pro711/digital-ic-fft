`timescale 1ns / 1ps

module cache_tb ();

        /* Parameter */
	parameter DATAWIDTH = 4;
	parameter NUM_POINT = 8;
	parameter LOG_POINT = 3;

	/* reg */
	reg en_read,en_write,rst,clk;
	reg signed [DATAWIDTH-1:0] data_write;
	reg signed [LOG_POINT-1:0] addr_read,addr_write;
	reg signed [1:0] state;

	wire signed [DATAWIDTH-1:0] data_read;
	
	initial begin
		clk <= 0;
		rst <= 1'b1;
		en_write <= 1'b0;
		en_read <= 1'b0;
		#20 rst <= 1'b0;
		#20 en_write <= 1'b1;
		#20 en_read <= 1'b1;
		#1000 $stop;
	end

	always #5 clk <= ~clk;

	always @(posedge clk) begin
		if (rst) begin
			addr_read <= 0;
			addr_write <= 0;
			data_write <= 0;
			state <= 0;
		end
		else if (en_write) begin
			case (state)
				2'b00:
				begin
					addr_write <= 1;
					data_write <= 4'b0101;
				end
				2'b01:
				begin
					addr_write <= 3;
					data_write <= 4'b0001;
				end
				2'b10:
				begin
					addr_write <= 7;
					data_write <= 4'b1001;
				end
				2'b11:
				begin
					addr_write <= 4;
					data_write <= 4'b1101;
				end
			endcase
		end
		else if (en_read) begin
			case (state)
				2'b00:
				begin
					addr_read <= 3;
				end
				2'b01:
				begin
					addr_read <= 4;
				end
				2'b10:
				begin
					addr_read <= 5;
				end
				2'b11:
				begin
					addr_read <= 3;
				end
			endcase
		end
	end

	cache c1( .en_read(en_read),
		  .addr_read(addr_read),
		  .data_read(data_read),
		  .en_write(en_write),
		  .addr_write(addr_write),
		  .data_write(data_write),
		  .rst(rst),
		  .clk(clk)
	  );

	  endmodule
