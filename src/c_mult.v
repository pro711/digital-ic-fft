/* coding: utf-8 */
/* vim:set et sts=2 ts=2 sw=2: */

/*
 * c_mult.v
 *  
 * Copyright (c) 2011 Tao Chen <08300720447@fudan.edu.cn>
 * Copyright (c) 2011 Peiqi Zheng <08300720459@fudan.edu.cn>
 *  
 */

`timescale 1ns/10ps

//==============================================================================
// general-purpose multiplier with configurable width
//==============================================================================

module c_mult(a, b, p);

  parameter width = 4;

  input signed [width-1:0] a;
  input signed [width-1:0] b;
  output signed [2*width-1:0] p;

  assign p = a * b;
  
endmodule
