/* coding: utf-8 */
/* vim:set et sts=2 ts=2 sw=2: */

/*
 * c_adder.v
 *  
 * Copyright (c) 2011 Tao Chen <08300720447@fudan.edu.cn>
 * Copyright (c) 2011 Peiqi Zheng <08300720459@fudan.edu.cn>
 *  
 */

`timescale 1ns/10ps

//==============================================================================
// general-purpose adder with configurable width
//==============================================================================

module c_adder(a, b, cin, s, cout);

  parameter width = 4;

  input [width-1:0] a;
  input [width-1:0] b;
  input cin;
  output [width-1:0] s;
  output cout;
  
  assign {cout, s} = a + b + cin;
  
endmodule

