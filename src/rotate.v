/* coding: utf-8 */
/* vim:set et sts=2 ts=2 sw=2: */

/*
 * rotate.v
 *  
 * Copyright (c) 2011 Tao Chen <08300720447@fudan.edu.cn>
 * Copyright (c) 2011 Peiqi Zheng <08300720459@fudan.edu.cn>
 *  
 */

`timescale 1ns/10ps

//==============================================================================
// rotate unit
//==============================================================================

module rotate(in_re, in_im, w, out_re, out_im);

  parameter data_width = 4;

  input [data_width-1:0] in_re;
  input [data_width-1:0] in_im;
  input w;  // twiddle factor: 0 : (1-j)/sqrt(2), 1 : (-1-j)/sqrt(2)
  
  output [data_width-1:0] out_re;
  output [data_width-1:0] out_im;

  wire [data_width-1:0] mult_1;
  wire [data_width-1:0] mult_2;
  wire [2*data_width-1:0] mult_p_1;
  wire [2*data_width-1:0] mult_p_2;

/*
  assign mult_1 = w ? (0-in_re-in_im) : (in_re-in_im);
  assign mult_2 = w ? (in_re-in_im) : (in_re+in_im);
*/
  // saturation operation
  wire [data_width-1:0] in_re_im_sum;
  wire [data_width-1:0] in_im_re_diff;
  wire [data_width-1:0] in_re_neg;
  wire [data_width-1:0] in_re_im_sum_neg;

  assign mult_1 = w ? in_im_re_diff : in_re_im_sum;
  assign mult_2 = w ? in_re_im_sum_neg : in_im_re_diff;
  
  c_negate
    #(.width(data_width))
  negate_in_re
    (.din(in_re),
    .dout(in_re_neg)
    );

  adder
    #(.width(data_width),
      .n_segment(1))
  add_in_re_in_im
    (.a(in_re),
     .b(in_im),
     .cin(1'b0),
     .s(in_re_im_sum)
    );

  adder
    #(.width(data_width),
      .n_segment(1))
  add_in_re_in_im_neg
    (.a(in_re_neg),
     .b(in_im),
     .cin(1'b0),
     .s(in_im_re_diff)
    );

  c_negate
    #(.width(data_width))
  negate_in_re_im_sum
    (.din(in_re_im_sum),
    .dout(in_re_im_sum_neg)
    );

  c_mult
    #(.width(data_width))
  mult_inst_1
    (.a(mult_1),
     .b(4'b0110),
     .p(mult_p_1));
  
  c_mult
    #(.width(data_width))
  mult_inst_2
    (.a(mult_2),
     .b(4'b0110),
     .p(mult_p_2));

  assign out_re = mult_p_1[2*data_width-2:data_width-1];
  assign out_im = mult_p_2[2*data_width-2:data_width-1];

endmodule

