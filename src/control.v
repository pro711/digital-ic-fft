/* coding: utf-8 */
/* vim:set et sts=2 ts=2 sw=2: */

/*
 * control.v
 *  
 * Copyright (c) 2011 Tao Chen <08300720447@fudan.edu.cn>
 * Copyright (c) 2011 Peiqi Zheng <08300720459@fudan.edu.cn>
 *  
 */

`timescale 1ns/10ps

//==============================================================================
// controller unit
//==============================================================================

module control(clk, rst_n, en, w, order, state, laststep);

//  parameter data_width = 4;

  input clk, rst_n;
  input en;
/*
  output [1:0] w [3:0];
  output [5:0] order [5:0];
*/
  output [2*4-1:0] w;
  output [6*6-1:0] order;
  output state;
  output laststep;
  reg [1:0] w_2d [3:0];
  reg [5:0] order_2d [5:0];
  assign w[1:0] = w_2d[0];
  assign w[3:2] = w_2d[1];
  assign w[5:4] = w_2d[2];
  assign w[7:6] = w_2d[3];
  assign order[5:0] = order_2d[0];
  assign order[11:6] = order_2d[1];
  assign order[17:12] = order_2d[2];
  assign order[23:18] = order_2d[3];
  assign order[29:24] = order_2d[4];
  assign order[35:30] = order_2d[5];

  wire [1:0] stage;
  reg [1:0] counter;
  
  localparam maxcount = 2;

  localparam STATE_READY = 1'b0;
  localparam STATE_COMP = 1'b1;
  reg state;

  // state machine
  always @(posedge clk or negedge rst_n)
  begin
    if (~rst_n)
    begin
      state <= STATE_READY;
      counter <= 0;
    end
    else
    begin
      case (state)
      STATE_READY:
      begin
        if (en)
        begin
          state <= STATE_COMP;
          counter <= 0;
        end
      end
      STATE_COMP:
      begin
        if (~laststep)
          counter <= counter + 1;
        else
        begin
          if (en)
          begin
            state <= STATE_COMP;
            counter <= 0;
          end
          else
          begin
            state <= STATE_READY;
            counter <= 0;
          end
        end
      end
      endcase
    end
  end

  // indicate last step of computation
  assign laststep = (counter == maxcount);

  // compute stages
  assign stage = counter;
/*
  always @(posedge clk or negedge rst_n)
  begin
    if (~rst_n)
    begin
      stage <= 0;
    end
    else
    begin
      if (counter == stage_1 || counter == stage_2 || counter == stage_3)
        stage <= stage + 1;
    end
  end
*/

  // twiddle factor
  always @(stage)
  begin
    case (stage)
    0:
    begin
      w_2d[0] = 2'b00;
      w_2d[1] = 2'b00;
      w_2d[2] = 2'b00;
      w_2d[3] = 2'b00;
    end
    1:
    begin
      w_2d[0] = 2'b00;
      w_2d[1] = 2'b01;
      w_2d[2] = 2'b00;
      w_2d[3] = 2'b01;
    end
    2:
    begin
      w_2d[0] = 2'b00;
      w_2d[1] = 2'b10;
      w_2d[2] = 2'b01;
      w_2d[3] = 2'b11;
    end
    default:
    begin
      w_2d[0] = 2'b00;
      w_2d[1] = 2'b00;
      w_2d[2] = 2'b00;
      w_2d[3] = 2'b00;
    end
    endcase
  end

  // reorder
  always @(stage)
  begin
    case (stage)
    0:
    begin
      order_2d[0] = 6'b000010;
      order_2d[1] = 6'b000001;
      order_2d[2] = 6'b000100;
      order_2d[3] = 6'b001000;
      order_2d[4] = 6'b100000;
      order_2d[5] = 6'b010000;
    end
    1:
    begin
      order_2d[0] = 6'b001000;
      order_2d[1] = 6'b000010;
      order_2d[2] = 6'b100000;
      order_2d[3] = 6'b000001;
      order_2d[4] = 6'b010000;
      order_2d[5] = 6'b000100;
    end
    2:
    begin
      order_2d[0] = 6'b000010;
      order_2d[1] = 6'b001000;
      order_2d[2] = 6'b100000;
      order_2d[3] = 6'b000001;
      order_2d[4] = 6'b000100;
      order_2d[5] = 6'b010000;
    end
    default:
    begin
      order_2d[0] = 6'b000010;
      order_2d[1] = 6'b000001;
      order_2d[2] = 6'b000100;
      order_2d[3] = 6'b001000;
      order_2d[4] = 6'b100000;
      order_2d[5] = 6'b010000;
    end
    endcase
  end
endmodule
