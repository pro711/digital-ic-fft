/* coding: utf-8 */
/* vim:set et sts=2 ts=2 sw=2: */

/*
 * mult_add.v
 *  
 * Copyright (c) 2011 Tao Chen <08300720447@fudan.edu.cn>
 * Copyright (c) 2011 Peiqi Zheng <08300720459@fudan.edu.cn>
 *  
 */

`timescale 1ns/10ps

//==============================================================================
// multiplier / adder unit
//==============================================================================

module mult_add(adder_a, adder_b, mult_a, mult_b, select, adder_s)

  parameter mult_width = 4;
  parameter adder_width = 16;
  parameter adder_n_segment = 4;

  input [adder_width-1:0] adder_a;
  input [adder_width-1:0] adder_b;
  input [mult_width-1:0] mult_a;
  input [mult_width-1:0] mult_b;
  input select;
  output [adder_width-1:0] adder_s;

  wire [adder_width-1:0] adder_in;
  wire cout;
  wire [2*mult_width-1:0] mult_p;

  c_mult
    #(.width(mult_width))
  mult_inst
    (.a(mult_a),
     .b(mult_b),
     .p(mult_p));

  assign adder_in = select ? adder_b :
    {(adder_width-2*mult_width){mult_p[2*mult_width-1]}, mult_p}; // sign extension
  
  adder
    #(.width(adder_width),
      .n_segment(adder_n_segment))
  add_inst
    (.a(adder_a),
     .b(adder_in),
     .cin(1'b0),
     .s(adder_s),
     .cout(cout)
    );
  

endmodule

