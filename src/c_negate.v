/* coding: utf-8 */
/* vim:set et sts=2 ts=2 sw=2: */

/*
 * c_neg.v
 *  
 * Copyright (c) 2011 Tao Chen <08300720447@fudan.edu.cn>
 * Copyright (c) 2011 Peiqi Zheng <08300720459@fudan.edu.cn>
 *  
 */

`timescale 1ns/10ps

//==============================================================================
// negate a signed number
//==============================================================================

module c_negate(din, dout);

  parameter width = 4;

  input [width-1:0] din;
  output [width-1:0] dout;

  // smallest negative number does not have a corresponding positive number
  // user largest positive number instead
  assign dout = (din == 1<<(width-1)) ? {1'b0, {(width-1){1'b1}}} : (~din+1);
  
endmodule

