/* coding: utf-8 */
/* vim:set et sts=2 ts=2 sw=2: */

/*
 * fft8.v
 *  
 * Copyright (c) 2011 Tao Chen <08300720447@fudan.edu.cn>
 * Copyright (c) 2011 Peiqi Zheng <08300720459@fudan.edu.cn>
 *  
 */

`timescale 1ns/10ps

//==============================================================================
// top module for 8-point FFT processor
//==============================================================================

module fft8(clk, rst_n, in, en, out, data_valid);

  parameter data_width = 4;

  input clk, rst_n;
  input [2*data_width*8-1:0] in;  // packed complex numbers format
  input en;
  output [2*data_width*8-1:0] out;
  reg [2*data_width*8-1:0] out;
  output data_valid;

  reg data_valid;
  reg [2*data_width-1:0] store [7:0];

  localparam STATE_READY = 1'b0;
  localparam STATE_COMP = 1'b1;

  wire [2*4-1:0] w;
  wire [6*6-1:0] order;
  wire [1:0] w_2d [3:0];
  wire [5:0] order_2d [5:0];
  assign w_2d[0] = w[1:0];
  assign w_2d[1] = w[3:2];
  assign w_2d[2] = w[5:4];
  assign w_2d[3] = w[7:6];
  assign order_2d[0] = order[5:0];
  assign order_2d[1] = order[11:6];
  assign order_2d[2] = order[17:12];
  assign order_2d[3] = order[23:18];
  assign order_2d[4] = order[29:24];
  assign order_2d[5] = order[35:30];

  wire [2*data_width-1:0] out_a [3:0];
  wire [2*data_width-1:0] out_b [3:0];

  wire state;
  wire laststep;
  wire [2*data_width*8-1:0] result; // intermediate result

/*
  operation of the FFT processor
  When [en] is asserted, the processor samples input from port [in].
  After computing FFT, output is written to [out] and [data_valid] is asserted.
*/
  
  // input
  always @(posedge clk or negedge rst_n)
  begin
    if (~rst_n)
    begin
      {store[7],store[6],store[5],store[4],store[3],store[2],store[1],store[0]} <= 0;
    end
    else
    begin
      if (en && (state == STATE_READY || (state == STATE_COMP && laststep)))
        // bit reverse
        {store[7],store[3],store[5],store[1],store[6],store[2],store[4],store[0]} <= in;
      else if (state == STATE_COMP && ~laststep)
        {store[7],store[6],store[5],store[4],store[3],store[2],store[1],store[0]} <= result;
    end
  end


  // controller
  control ctrl (.clk(clk), .rst_n(rst_n), .en(en), .w(w), .order(order), .state(state), .laststep(laststep));

  // 4-way butterfly units
  butterfly
    #(.data_width(data_width),
      .enable_rotate(0))
  bf_0
    (.in_a(store[0]), .in_b(store[1]), .w(w_2d[0]), .out_a(out_a[0]), .out_b(out_b[0]));

  butterfly
    #(.data_width(data_width),
      .enable_rotate(1))
  bf_1
    (.in_a(store[2]), .in_b(store[3]), .w(w_2d[1]), .out_a(out_a[1]), .out_b(out_b[1]));

  butterfly
    #(.data_width(data_width),
      .enable_rotate(0))
  bf_2
    (.in_a(store[4]), .in_b(store[5]), .w(w_2d[2]), .out_a(out_a[2]), .out_b(out_b[2]));

  butterfly
    #(.data_width(data_width),
      .enable_rotate(1))
  bf_3
    (.in_a(store[6]), .in_b(store[7]), .w(w_2d[3]), .out_a(out_a[3]), .out_b(out_b[3]));

  wire [2*data_width*6-1:0] xbar_in;
  wire [2*data_width*6-1:0] xbar_out;
/*
  wire [2*data_width-1:0] xbar_in_2d [5:0];
  wire [2*data_width-1:0] xbar_out_2d [5:0];
*/
  assign xbar_in[2*data_width*1-1:0] = out_b[0];
  assign xbar_in[2*data_width*2-1:2*data_width*1] = out_a[1];
  assign xbar_in[2*data_width*3-1:2*data_width*2] = out_b[1];
  assign xbar_in[2*data_width*4-1:2*data_width*3] = out_a[2];
  assign xbar_in[2*data_width*5-1:2*data_width*4] = out_b[2];
  assign xbar_in[2*data_width*6-1:2*data_width*5] = out_a[3];
  
  // crossbar
  crossbar #(.width(2*data_width)) xbar (.order(order), .in(xbar_in), .out(xbar_out));

  // output
/*
  assign out[0] = out_a[0];
  assign out[1] = xbar_out[2*data_width*1-1:0];
  assign out[2] = xbar_out[2*data_width*2-1:2*data_width*1];
  assign out[3] = xbar_out[2*data_width*3-1:2*data_width*2];
  assign out[4] = xbar_out[2*data_width*4-1:2*data_width*3];
  assign out[5] = xbar_out[2*data_width*5-1:2*data_width*4];
  assign out[6] = xbar_out[2*data_width*6-1:2*data_width*5];  
  assign out[7] = out_b[3];
*/

  assign result = {out_b[3], xbar_out, out_a[0]};

  // output
  always @(posedge clk or negedge rst_n)
  begin
    if (~rst_n)
      data_valid <= 1'b0;
    else if (laststep)
    begin
      data_valid <= 1;
      out <= result;
    end
    else
      data_valid <= 0;
  end

endmodule
