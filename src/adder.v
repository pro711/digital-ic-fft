/* coding: utf-8 */
/* vim:set et sts=2 ts=2 sw=2: */

/*
 * adder.v
 *  
 * Copyright (c) 2011 Tao Chen <08300720447@fudan.edu.cn>
 * Copyright (c) 2011 Peiqi Zheng <08300720459@fudan.edu.cn>
 *  
 */

`timescale 1ns/10ps

//==============================================================================
// SIMD adder with configurable width and segment, and saturation operation
//==============================================================================

module adder(a, b, cin, s, cout);

  parameter width = 16;
  parameter n_segment = 4;

  localparam seg_width = width / n_segment;

  input [width-1:0] a;
  input [width-1:0] b;
  input cin;
  output [width-1:0] s;
  output cout;

  generate
    genvar i;
    for (i = 0; i < n_segment; i = i + 1)
    begin:segment
      wire [seg_width-1:0] seg_s;
      wire seg_cout;
      wire sign_a;
      wire sign_b;
      wire sign_s;
      wire overflow;
      
      c_adder
        #(.width(seg_width))
      segment_adder
        (.a(a[(i+1)*seg_width-1:i*seg_width]),
         .b(b[(i+1)*seg_width-1:i*seg_width]),
         .cin(1'b0),
         .s(seg_s),
         .cout(seg_cout)
         );
      assign sign_a = a[(i+1)*seg_width-1];
      assign sign_b = b[(i+1)*seg_width-1];
      assign sign_s = seg_s[seg_width-1];
      assign overflow = (sign_a == sign_b) && (sign_a != sign_s);
      // saturation logic
      assign s[(i+1)*seg_width-1:i*seg_width] = overflow ? {~sign_s, {(seg_width-1){sign_s}}} : seg_s;
    end
  endgenerate
  
endmodule
