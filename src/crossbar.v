/* coding: utf-8 */
/* vim:set et sts=2 ts=2 sw=2: */

/*
 * crossbar.v
 *  
 * Copyright (c) 2011 Tao Chen <08300720447@fudan.edu.cn>
 * Copyright (c) 2011 Peiqi Zheng <08300720459@fudan.edu.cn>
 *  
 */

`timescale 1ns/10ps

//==============================================================================
// 6x6 crossbar with configurable width
//==============================================================================

module crossbar(order, in, out);

  parameter width = 8;
  
  input [6*6-1:0] order;
  input [width*6-1:0] in;
  output [width*6-1:0] out;

  // packing / unpacking of 2D array
  wire [5:0] order_2d [5:0];
  assign {order_2d[5],order_2d[4],order_2d[3],order_2d[2],order_2d[1],order_2d[0]} = order;
  wire [width-1:0] in_2d [5:0];
  assign {in_2d[5],in_2d[4],in_2d[3],in_2d[2],in_2d[1],in_2d[0]} = in;
  reg [width-1:0] out_2d [5:0];
  assign out = {out_2d[5],out_2d[4],out_2d[3],out_2d[2],out_2d[1],out_2d[0]};

  always @(*)
  begin
    case (order_2d[0] & 6'b001010)
    6'b000001:
      out_2d[0] = in_2d[0];
    6'b000010:
      out_2d[0] = in_2d[1];
    6'b000100:
      out_2d[0] = in_2d[2];
    6'b001000:
      out_2d[0] = in_2d[3];
    6'b010000:
      out_2d[0] = in_2d[4];
    6'b100000:
      out_2d[0] = in_2d[5];
    default:
      out_2d[0] = in_2d[0];
    endcase
  end

  always @(*)
  begin
    case (order_2d[1] & 6'b001011)
    6'b000001:
      out_2d[1] = in_2d[0];
    6'b000010:
      out_2d[1] = in_2d[1];
    6'b000100:
      out_2d[1] = in_2d[2];
    6'b001000:
      out_2d[1] = in_2d[3];
    6'b010000:
      out_2d[1] = in_2d[4];
    6'b100000:
      out_2d[1] = in_2d[5];
    default:
      out_2d[1] = in_2d[1];
    endcase
  end

  always @(*)
  begin
    case (order_2d[2] & 6'b100100)
    6'b000001:
      out_2d[2] = in_2d[0];
    6'b000010:
      out_2d[2] = in_2d[1];
    6'b000100:
      out_2d[2] = in_2d[2];
    6'b001000:
      out_2d[2] = in_2d[3];
    6'b010000:
      out_2d[2] = in_2d[4];
    6'b100000:
      out_2d[2] = in_2d[5];
    default:
      out_2d[2] = in_2d[2];
    endcase
  end

  always @(*)
  begin
    case (order_2d[3] & 6'b001001)
    6'b000001:
      out_2d[3] = in_2d[0];
    6'b000010:
      out_2d[3] = in_2d[1];
    6'b000100:
      out_2d[3] = in_2d[2];
    6'b001000:
      out_2d[3] = in_2d[3];
    6'b010000:
      out_2d[3] = in_2d[4];
    6'b100000:
      out_2d[3] = in_2d[5];
    default:
      out_2d[3] = in_2d[3];
    endcase
  end

  always @(*)
  begin
    case (order_2d[4] & 6'b110100)
    6'b000001:
      out_2d[4] = in_2d[0];
    6'b000010:
      out_2d[4] = in_2d[1];
    6'b000100:
      out_2d[4] = in_2d[2];
    6'b001000:
      out_2d[4] = in_2d[3];
    6'b010000:
      out_2d[4] = in_2d[4];
    6'b100000:
      out_2d[4] = in_2d[5];
    default:
      out_2d[4] = in_2d[4];
    endcase
  end

  always @(*)
  begin
    case (order_2d[5] & 6'b010100)
    6'b000001:
      out_2d[5] = in_2d[0];
    6'b000010:
      out_2d[5] = in_2d[1];
    6'b000100:
      out_2d[5] = in_2d[2];
    6'b001000:
      out_2d[5] = in_2d[3];
    6'b010000:
      out_2d[5] = in_2d[4];
    6'b100000:
      out_2d[5] = in_2d[5];
    default:
      out_2d[5] = in_2d[5];
    endcase
  end

endmodule
