`timescale 1ns / 1ps
/******************************************************************************
   PROJECT:  FFT Processor - Single Butterfly Version
   MODULE:   Cache

   DESCRIPTION: 
   
   This is a single Cache module.
   
   PARAMETERS: 
   
   DATAWIDTH       : Width of single entry
   NUM_POINT       : FFT Point. Power of 2  
   
   INPUTS:    
  
   en_read    : Read enable(high active)
   addr_read  : Address to read from Cache
   en_write   : Write enable(high active)
   addr_write : Address to write to Cache
   data_write : Data to write to Cache
   rst        : Reset(high active,asynchronous)
   clk        : Clock
    
   OUTPUTS:   
   
   data_read  : Data to read from Cache

******************************************************************************/


module cache
(
	en_read,
	addr_read,
	data_read,
	en_write,
	addr_write,
	data_write,
	rst,
	clk
);

        /* Parameter */
	parameter DATAWIDTH = 4;
	parameter NUM_POINT = 8;
	parameter LOG_POINT = 3;

	/* Input */
	input en_read,en_write,rst,clk;
	input signed [DATAWIDTH-1:0] data_write;
	input signed [LOG_POINT-1:0] addr_read,addr_write;

	/* Output */
	output signed [DATAWIDTH-1:0] data_read;
	
	reg [DATAWIDTH-1:0] mem [0:NUM_POINT-1];
	reg [LOG_POINT-1:0] initaddr;

	always @ (posedge clk) begin
		if (rst) begin
			for ( initaddr = 0; initaddr < NUM_POINT; initaddr = initaddr + 1) begin
				mem[initaddr] <= 0;
			end
		end
		else if ( en_write ) begin
			mem[addr_write] <= data_write;
		end
	end

	assign data_read = en_read ? ( ( addr_read == addr_write ) ? data_write : mem[addr_read] ) : 0;

	endmodule
