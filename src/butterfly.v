/* coding: utf-8 */
/* vim:set et sts=2 ts=2 sw=2: */

/*
 * butterfly.v
 *  
 * Copyright (c) 2011 Tao Chen <08300720447@fudan.edu.cn>
 * Copyright (c) 2011 Peiqi Zheng <08300720459@fudan.edu.cn>
 *  
 */

`timescale 1ns/10ps

//==============================================================================
// butterfly unit
//==============================================================================

module butterfly(in_a, in_b, w, out_a, out_b);

  parameter data_width = 4;
  parameter enable_rotate = 1;
  localparam adder_n_segment = 4;
  localparam adder_width = data_width * adder_n_segment;

  input [2*data_width-1:0] in_a;  // packed input: {Re, Im}
  input [2*data_width-1:0] in_b;
  input [1:0] w;  // encoded rotation factor: 00 : 1, 01 : -j
                  //    10 : (1-j)/sqrt(2), 11 : (-1-j)/sqrt(2)
  output [2*data_width-1:0] out_a;
  output [2*data_width-1:0] out_b;


/*
  c_mult
    #(.width(mult_width))
  mult_inst
    (.a(mult_a),
     .b(mult_b),
     .p(mult_p));
*/
  // multiply with twiddle factor
  wire [2*data_width-1:0] in_b_wn;
  generate
    if (enable_rotate == 1)
    begin
      rotate
        #(.data_width(data_width))
      rotate_inst
        (.in_re(in_b[2*data_width-1:data_width]),
        .in_im(in_b[data_width-1:0]),
        .w(w[0]),
        .out_re(in_b_wn[2*data_width-1:data_width]),
        .out_im(in_b_wn[data_width-1:0]));
    end
    else
    begin
      assign in_b_wn = 0;
    end
  endgenerate
    
/*
  assign adder_in = select ? adder_b :
    {(adder_width-2*mult_width){mult_p[2*mult_width-1]}, mult_p}; // sign extension
*/

  wire [4*data_width-1:0] adder_a, adder_b, adder_s;
  wire [2*data_width-1:0] /*in_a_neg,*/ in_b_neg;
/*
  c_negate
    #(.width(data_width))
  negate_a_re
    (.din(in_a[data_width-1:0]),
    .dout(in_a_neg[data_width-1:0]));

  c_negate
    #(.width(data_width))
  negate_a_im
    (.din(in_a[2*data_width-1:data_width]),
    .dout(in_a_neg[2*data_width-1:data_width]));
*/
  
  c_negate
    #(.width(data_width))
  negate_b_re
    (.din(w[1] ? in_b_wn[data_width-1:0] : in_b[data_width-1:0]),
    .dout(in_b_neg[data_width-1:0]));
  
  c_negate
    #(.width(data_width))
  negate_b_im
    (.din(w[1] ? in_b_wn[2*data_width-1:data_width] : in_b[2*data_width-1:data_width]),
    .dout(in_b_neg[2*data_width-1:data_width]));

  assign adder_a = {in_a, in_a};
  assign adder_b = (w == 2'b00) ? {in_b, in_b_neg} :
    (w == 2'b01) ? {{in_b[data_width-1:0], in_b_neg[2*data_width-1:data_width]},
                    {in_b_neg[data_width-1:0], in_b[2*data_width-1:data_width]}} : {in_b_wn , in_b_neg};
  
  adder
    #(.width(adder_width),
      .n_segment(adder_n_segment))
  add_inst
    (.a(adder_a),
     .b(adder_b),
     .cin(1'b0),
     .s(adder_s),
     .cout(cout)
    );

  assign {out_a, out_b} = adder_s;

endmodule
